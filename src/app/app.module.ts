import { EnvironmentInjector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AppComponent } from './app.component';
import { RegionsComponent } from './regions/regions.component';
import { CountriesComponent } from './countries/countries.component';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { regionReducer } from './store/region/region.reducer';
import { countryReducer } from './store/country/country.reducer';
import { RegionEffects } from './store/region/region.effects';
import { CountryEffects } from './store/country/country.effects';
import { CountryService } from './services/country.service';
import { RegionService } from './services/region.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DropdownComponent } from './dropdown.component';
import { FormsModule } from '@angular/forms';
import { StoreDevtools, StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
  declarations: [
    AppComponent,
    DropdownComponent,
    RegionsComponent,
    CountriesComponent,
    CountryDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot({ region: regionReducer, country: countryReducer }),
    StoreDevtoolsModule.instrument({maxAge:25}),
    EffectsModule.forRoot([RegionEffects, CountryEffects]),
    NgbModule
  ],
  providers: [CountryService, RegionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
