import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Country } from '../store/country/country.model';
import { loadCountriesByRegion, selectCountry } from '../store/country/country.actions';
import { selectCountriesByRegion, selectSelectedCountry } from '../store/country/country.selectors';
import { selectSelectedRegion } from '@app/store/region/region.selectors';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit, OnDestroy {
  countries$: Observable<Country[]>;
  regions: string[] = ['asia', 'europe'];
  region!: Observable<string>;
  selectedCountry: Country | null = null;

  private destroy$: Subject<void> = new Subject<void>();

  constructor(private store: Store) {
    this.countries$ = this.store.pipe(select(selectCountriesByRegion));
  }

  ngOnInit(): void {
    this.region = this.store.select(selectSelectedRegion);
    this.region.subscribe(region => {
      
      this.store.dispatch(loadCountriesByRegion({ region }));
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onCountryChange(country: string): void {
    // this.selectedCountry = country;
    // console.log(this.selectedCountry);

    this.store.dispatch(selectCountry({ country}));
  }
}
