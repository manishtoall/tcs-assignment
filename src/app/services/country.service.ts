import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Country } from '@app/store/country/country.model';

@Injectable()
export class CountryService {
  constructor(private http: HttpClient) {}

  getCountriesByRegion(region: string): Observable<Country[]> {
    return this.http.get<any[]>(`https://restcountries.com/v2/region/${region}`).pipe(
      map(response => response.map(data => ({
        name: data.name,
        capital: data.capital,
        population: data.population,
        currencies: data.currencies,
        flags: data.flags
      })))
    )
  }
}
