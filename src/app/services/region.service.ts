import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class RegionService {
  constructor(private http: HttpClient) {}

  getRegions(): Observable<string[]> {
    return this.http.get<any[]>('https://restcountries.com/v2/all').pipe(
      map(response => {
        const regionsSet = new Set<string>();
        response.forEach(country => {
          if (country.region) {
            regionsSet.add(country.region);
          }
        });
        return Array.from(regionsSet);
      })
    );
  }
}
