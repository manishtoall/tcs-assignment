import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  template: `
    <label>{{ label }}</label>
    <select [(ngModel)]="selectedItem">
      <option *ngFor="let item of items" [value]="item">{{ item }}</option>
    </select>
  `,
})
export class DropdownComponent {
  @Input() label!: string;
  @Input() items!: string[];
  selectedItem!: string;
}
