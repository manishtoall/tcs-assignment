import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectRegions, selectSelectedRegion } from '../store/region/region.selectors';
import { changeSelectedRegion } from '../store/region/region.actions';

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.css']
})
export class RegionsComponent implements OnInit {
  regions$: Observable<string[]>;
  selectedRegion$: Observable<string>;

  constructor(private store: Store) {
    this.regions$ = this.store.pipe(select(selectRegions));
    this.selectedRegion$ = this.store.pipe(select(selectSelectedRegion));
  }

  ngOnInit(): void {
    // Dispatch an action to load the regions from the store
    // You can update this based on how the regions are loaded in your application
  }

  onRegionChange(region: string): void {
    this.store.dispatch(changeSelectedRegion({ region }));
  }
}
