import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Country } from '../store/country/country.model';
import { selectSelectedCountry, selectCountriesByRegion } from '../store/country/country.selectors';
// import { selectCountriesByRegion } from '../store/country/country.selectors';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css']
})
export class CountryDetailsComponent implements OnInit, OnDestroy {
  selectedCountry$: Observable<Country | null>;

  private destroy$: Subject<void> = new Subject<void>();

  constructor(private store: Store<{ country: { selectedCountry: Country | null } }>) {
    this.selectedCountry$ = this.store.pipe(select(selectSelectedCountry));
  }

  ngOnInit(): void {
    this.selectedCountry$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(country => {
      console.log('Selected Country:', JSON.stringify(country));
      console.log('Selected Country - Name:', country?.name);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
