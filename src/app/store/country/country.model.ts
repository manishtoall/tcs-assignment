export interface Country {
    name: string;
    capital: string;
    population: number;
    currencies: Currency[];
    flags: string[];
  }
  
  export interface Currency {
    code: string;
    name: string;
    symbol: string;
  }