import { createSelector, createFeatureSelector } from '@ngrx/store';
import { Country } from './country.model';
import { CountryState } from './country.reducer';

export const selectCountryState = createFeatureSelector<CountryState>('country');

export const selectCountriesByRegion = createSelector(
  selectCountryState,
  (state: CountryState) => state.countriesByRegion
);

export const selectSelectedCountry = createSelector(
  selectCountryState,
  (state: CountryState) => state.selectedCountry
);
