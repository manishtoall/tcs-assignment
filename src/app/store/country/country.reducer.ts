import { createReducer, on, Action } from '@ngrx/store';
import { loadCountriesByRegionSuccess, selectCountry } from './country.actions';
import { Country } from './country.model';

export interface CountryState {
  countriesByRegion: Country[];
  selectedCountry: Country | null;
}

export const initialState: CountryState = {
  countriesByRegion: [],
  selectedCountry: null
};

export const countryReducer = createReducer(
  initialState,
  on(loadCountriesByRegionSuccess, (state, { countries }) => ({
    ...state,
    countriesByRegion: countries
  })),
  on(selectCountry, (state, { country }) => ({
    ...state,
    selectedCountry: state.countriesByRegion.find(countryObj=>countryObj.name===country) || null
  }))
);

export function reducer(state: CountryState | undefined, action: Action) {
  return countryReducer(state, action);
}
