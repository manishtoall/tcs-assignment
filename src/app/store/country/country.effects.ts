import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { CountryService } from '../../services/country.service';
import { loadCountriesByRegion, loadCountriesByRegionFailure, loadCountriesByRegionSuccess } from './country.actions';

@Injectable()
export class CountryEffects {
  loadCountriesByRegion$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadCountriesByRegion),
      mergeMap(({ region }) =>
        this.countryService.getCountriesByRegion(region).pipe(
          map(countries => loadCountriesByRegionSuccess({ countries })),
          catchError(error => of(loadCountriesByRegionFailure({ error })))
        )
      )
    )
  );

  constructor(private actions$: Actions, private countryService: CountryService) {}
}
