import { createAction, props } from '@ngrx/store';
import { Country } from './country.model';

export const loadCountriesByRegion = createAction('[Country] Load Countries By Region', props<{ region: string }>());
export const loadCountriesByRegionSuccess = createAction(
  '[Country] Load Countries By Region Success',
  props<{ countries: Country[] }>()
);
export const loadCountriesByRegionFailure = createAction('[Country] Load Countries By Region Failure', props<{ error: any }>());
// export const selectCountry = createAction('[Country] Select Country', props<{ country: Country }>());
export const selectCountry = createAction('[Country] Select Country', props<{ country: string | null }>());