import { createAction, props } from '@ngrx/store';

export const loadRegions = createAction('[Region] Load Regions');
export const loadRegionsSuccess = createAction('[Region] Load Regions Success', props<{ regions: string[] }>());
export const changeSelectedRegion = createAction('[Region] Change Selected Region', props<{ region: string }>());
