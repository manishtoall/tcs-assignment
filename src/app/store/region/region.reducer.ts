import { createReducer, on } from '@ngrx/store';
import { loadRegionsSuccess, changeSelectedRegion } from './region.actions';

export interface RegionState {
  regions: string[];
  selectedRegion: string;
}

export const initialState: RegionState = {
  regions: ['Europe', 'Asia'], // Hardcoded regions
  selectedRegion: ''
};

export const regionReducer = createReducer(
  initialState,
  on(loadRegionsSuccess, (state, { regions }) => ({ ...state, regions })),
  on(changeSelectedRegion, (state, { region }) => ({ ...state, selectedRegion: region }))
);
