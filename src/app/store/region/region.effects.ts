import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { mergeMap, map } from 'rxjs/operators';
import { RegionService } from '../../services/region.service';
import { loadRegions, loadRegionsSuccess } from './region.actions';

@Injectable()
export class RegionEffects {
  loadRegions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadRegions),
      mergeMap(() =>
        this.regionService.getRegions().pipe(
          map(regions => loadRegionsSuccess({ regions }))
        )
      )
    )
  );

  constructor(private actions$: Actions, private regionService: RegionService) {}
}


