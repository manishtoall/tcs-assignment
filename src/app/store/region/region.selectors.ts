import { createSelector, createFeatureSelector } from '@ngrx/store';
import { RegionState } from './region.reducer';

export const selectRegionState = createFeatureSelector<RegionState>('region');

export const selectRegions = createSelector(
  selectRegionState,
  (state: RegionState) => state.regions
);

export const selectSelectedRegion = createSelector(
  selectRegionState,
  (state: RegionState) => state.selectedRegion
);
