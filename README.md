# CountryExplorerAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.4.

• Any style guides you used for your coding
I have used angular bootstrap for it but trying to apply simple anguarl functionality to it

• Any CSS coding standards
Bootstrap for table and dropdowns

• Installation instructions
Download my code and just run the command NPM install

• Description of how the application work
you need to run the command ng serve and it will open your local on 4200 port
you can select the dropdown of region to any country

Store:
When you will select the resgion I am getting the country list whhich is getting stored in Country store. after selected the country I am fetching selected country and storing it to the selectedCountry property to country store and displaying it to table in country details component.

• If you ran out of time, what else you would have done.

I would add any CSS file and remove unwanted code which is not required.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
